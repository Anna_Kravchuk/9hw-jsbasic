// <!--    ТЕОРЕТИЧНІ ПИТАННЯ

// 1.Опишіть, як можна створити новий HTML тег на сторінці.
// document.createElement(tag)- створює елемент із тегом
// та document.createTextNode - створює текстовий вузел із тегом

// 2.Опишіть, що означає перший параметр функції insertAdjacentHTML i опишіть можливі варіанти 
// цього параметра.
// 1й парметр вказує куди буде зроблена вставка по відношенню до еlem
// beforebegin - перед еlem,
// afterbegin - на початок еlem,
// beforeend - напрікінці еlem,
// afterend - після еlem,
 
// 3.Як можна видалити елемент зі сторінки?
// зявдяки методу node.remove()
// -->


//    ПРАКТИЧНЕ ЗАВДАННЯ

// Реалізувати функцію, яка отримуватиме масив елементів i виводити
// їх на сторінку у вигляді списку.

// Технічні вимоги:
// 1.Створити функцію, яка прийматиме на вхід масив i опціональний другий аргумент parent - DOM-елемент,
// до якого буде прикріплений список (по дефолту має бути document.body.
// 2.Кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

// Приклади масивів, які можна виводити на екран:
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];
// Можна взяти будь-який інший масив.

// ------------

// let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// let parent;

// function createList(arr , parent = document.body ){

//      const newUl = document.createElement('ul');

//      arr.forEach( elem => {
//           const newLi =  document.createElement('li');
//           newLi.textContent= elem;
//           newUl.appendChild(newLi);
//      })

//      parent.appendChild(newUl);
// }

// createList(arr);  

// -------------


let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let parent;

function createList(arr , parent = document.body ){

     const newUl = document.createElement('ul');

     arr.forEach( elem => {
          const newLi =  document.createElement('li');
          newLi.textContent= elem;
          newUl.appendChild(newLi);
     })

     parent.appendChild(newUl);
}

createList(arr);  


// ----------------
// Необов'язкове завдання підвищеної складності:
// Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, 
// виводити його як вкладений список. Приклад такого масиву:
// ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
// Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
// Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) 
// перед очищенням сторінки.


// -----------------

// for (let i=0 ; i< arr.length; i++){
//     console.log(arr[i]);
// }

// for (let elem of arr){
//     console.log(elem);
// }

// arr.forEach((elem)=>{
//     console.log(elem);
// })